################################################################################
#
# ddcmp
#
################################################################################

DDCMP_VERSION = 0da08ae9d896f179dccaed17a33b11245db0a13e
DDCMP_SOURCE = ddcmp-$(DDCMP_VERSION).zip
DDCMP_SITE = http://github.com/siro20/ddcmp/archive/$(DDCMP_VERSION)
DDCMP_LICENSE = BSD
DDCMP_LICENSE_FILES = ddcmp.c

DDCMP_MAKE_OPTS = \
	CC="$(TARGET_CC)"

define DDCMP_EXTRACT_CMDS
	$(UNZIP) -d $(@D) $(DDCMP_DL_DIR)/$(DDCMP_SOURCE)
	mv $(@D)/ddcmp-$(DDCMP_VERSION)/* $(@D)
	$(RM) -r $(@D)/ddcmp-$(DDCMP_VERSION)
endef

define DDCMP_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) CC="$(TARGET_CC)"
endef

define DDCMP_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) PREFIX="/usr/" DESTDIR="$(TARGET_DIR)" install
endef

$(eval $(generic-package))
